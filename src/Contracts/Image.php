<?php

declare(strict_types=1);

namespace Enfuse\Contracts;

interface Image
{

    /*
     * get array with image
     *
     * return @array
     */
    public function get():array;

    //public function check();

}
<?php

namespace Enfuse;

use Enfuse\Contracts\Image as ImageProcess;

class Image
{

  /*
   * exemplar of class
   * @var \Enfuse\Contracts\Image
   */
   protected $image;

   /*
    *
    */
   public function __construct(ImageProcess $image){
     $this->image = $image;
   }

   public function countImage()
   {
     return count($this->image->get());
   }

   /*
    * return avalible output file
    *
    * @return array
    */
   public function getAvalibleOutputFile():array
   {
     return $this->image->get();
   }

   public function two(){

   }
}
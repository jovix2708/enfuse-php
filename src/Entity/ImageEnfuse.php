<?php

namespace Enfuse\Entity;

use Enfuse\Contracts\Image;

class ImageEnfuse implements Image
{
    /*
     * @var $dwnPath
     * @type string
     * download path (working directory from Symfony\Proccess)
     */
    protected $dwnPath;

    /*
     * @var $output
     * @type array
     * Output and input image
     */
    protected $output;


    public function __construct(string $downloadPath, string $output )
    {
        $this->dwnPath = $downloadPath;
        $this->output  = $this->convertToArray($output);
    }

    public function get():array
    {
        $arrayOutputFile = []; // array with avalible file.

        if (is_array($this->output['output'])) {
            foreach ($this->output['output'] as $item => $value) {
                if (file_exists($this->dwnPath . '/' . $value)) {
                    $arrayOutputFile[] = $value;
                }
            }
        }elseif(file_exists($this->dwnPath .'/'. $this->output['output'])) {
            $arrayOutputFile[] = $this->output['output'];
        }

        return $arrayOutputFile;
    }

    public function check()
    {

    }

    /*
     * array with input and output
     *
     * @return array
     */
    private function convertToArray(string $string): array
    {
      $arrayFromString = explode(' ', $string);

      $output = array_shift($arrayFromString); //get and delete first element from array

      return [
          'output' => $output,
          'input'  => $arrayFromString
      ];
    }



}